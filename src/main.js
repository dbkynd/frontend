import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import vuetify from "./plugins/vuetify";
import VueResource from "vue-resource";
import ImgGen from "@/ImgGen";

export const imgGen = new ImgGen();

Vue.use(VueResource);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
