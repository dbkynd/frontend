let verifiedSeeds;
let notVerifiedSeeds;

export default {
  methods: {
    async getVerifiedSeeds() {
      if (!verifiedSeeds) {
        const response = await this.$http({
          method: "GET",
          url: "/api/verified"
        });
        verifiedSeeds = response.body;
      }

      return verifiedSeeds;
    },
    async getVerifiedAllTmpSeeds() {
      if (!verifiedSeeds) {
        const response = await this.$http({
          method: "GET",
          url: "/api/verified?limit=90000&page=0"
        });
        verifiedSeeds = response.body;
      }

      return verifiedSeeds;
    },
    async getNotVerifiedSeeds() {
      if (!notVerifiedSeeds) {
        const response = await this.$http({
          method: "GET",
          url: "/api/notverified"
        });
        notVerifiedSeeds = response.body;
      }

      return notVerifiedSeeds;
    },
    async getSeed(seedID) {
      const response = await this.$http({
        method: "GET",
        url: "/api/seed/" + seedID
      });

      if (response.ok) {
        return response.body;
      }

      return null;
    },
    async getSPS() {
      const response = await this.$http({
        method: "GET",
        url: "/statistics/api/sps"
      });

      if (response.ok) {
        return this.formatNumber(response.body.seeds_per_secound);
      }

      return null;
    },
    async getTotalScanned() {
      const response = await this.$http({
        method: "GET",
        url: "/statistics/api/totalscanned"
      });

      if (response.ok) {
        return this.formatNumber(response.body.total_scanned_seeds);
      }

      return null;
    },
    async getTotalViable() {
      const response = await this.$http({
        method: "GET",
        url: "/statistics/api/totalviable"
      });

      if (response.ok) {
        return this.formatNumber(response.body.total_viable_seeds);
      }

      return null;
    },

    // Prep for all statistics endpoint
    // NOT YET AVAILABLE
    async getStatistics() {
      const response = await this.$http({
        method: "GET",
        url: "/statistics/api/all"
      });

      if (response.ok) {
        // refactor the response body to have original and formatted properties
        // in case we want to use original somewhere else
        for (const stat in response.body) {
          if (Object.prototype.hasOwnProperty.call(response.body, stat)) {
            response.body[stat] = {
              original: response.body[stat],
              formatted: this.formatNumber(response.body[stat])
            };
          }
        }
        return response.body;
      }

      return null;
    },

    formatNumber(number) {
      if (number < 1.0e3) {
        return number;
      }
      if (number < 1.0e6) {
        return this.reduceDecimals(number / 1.0e3) + " K";
      }
      if (number < 1.0e9) {
        return this.reduceDecimals(number / 1.0e6) + " M";
      }
      if (number < 1.0e12) {
        return this.reduceDecimals(number / 1.0e9) + " B";
      }

      return this.reduceDecimals(number / 1.0e12) + " T";
    },
    reduceDecimals(number) {
      return ((number * 1000).toFixed() / 1000).toLocaleString();
    }
  }
};
