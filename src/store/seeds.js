import axios from "axios";

const numOfSeedsToFetchPerPage = 50;

export default {
  namespaced: true,

  state: {
    items: [],
    currentPage: 0,
    totalResults: 0,
    isFetchingSeeds: false,
    fetchError: false,
    noMoreResults: false
  },

  mutations: {
    setItems(state, seeds) {
      state.items = seeds;
    },
    addSeeds(state, seeds) {
      state.items.push(...seeds);
    },
    incrementPage(state) {
      state.currentPage++;
    },
    setTotalResults(state, total) {
      state.totalResults = total;
    },
    setIsFetchingSeeds(state, bool) {
      state.isFetchingSeeds = bool;
    },
    resetPagination(state) {
      state.currentPage = 0;
    },
    setFetchError(state, bool) {
      state.fetchError = bool;
    },
    setNoMoreResults(state, bool) {
      state.noMoreResults = bool;
    }
  },

  actions: {
    setItems({ commit }, payload) {
      commit("setItems", payload);
    },
    async getSeeds({ state, rootGetters, commit }) {
      // Dont do anything if we are already making a request
      if (state.isFetchingSeeds || state.noMoreResults) return;

      commit("setIsFetchingSeeds", true);
      // const filterOptions = rootGetters["filters/filterOptions"];
      const apiFilterBody = rootGetters["filters/apiFilterBody"];
      let data;
      // Try to make the api request
      try {
        const response = await axios.post("/api/verified", apiFilterBody, {
          params: { limit: numOfSeedsToFetchPerPage, page: state.currentPage }
        });
        // const response = await axios.get("/api/verified", {
        //   data: apiFilterBody,
        //   params: { limit: numOfSeedsToFetchPerPage, page: state.currentPage }
        // });
        data = response.data;
        // Clear the fetch error if there was one now that we have good results
        if (state.fetchError) commit("setFetchError", false);
      } catch (e) {
        // 4xx and 5xx errors
        commit("setFetchError", true);
      }
      // No data; Either from error or null results from the database
      // Clear and reset items; then exit
      if (!data.total) {
        commit("setTotalResults", 0);
        commit("setItems", []);
        commit("setIsFetchingSeeds", false);
        return;
      }

      if (!data.seeds) {
        commit("setNoMoreResults", true);
        commit("setIsFetchingSeeds", false);
        return;
      }

      const flattened = flattenSeeds(data.seeds || []);
      commit("addSeeds", flattened);

      commit("incrementPage");
      commit("setTotalResults", data.total);
      commit("setIsFetchingSeeds", false);
    },
    async getSeedsAdvanced({ state, rootGetters, commit }) {

      if (state.isFetchingSeeds || state.noMoreResults) return;

      commit("setIsFetchingSeeds", true);

      const apiFilterBody = rootGetters["filters/apiFilterBodyAdvanced"];
      let data;

      try {
        const response = await axios.post("/api/verified", apiFilterBody, {
          params: { limit: numOfSeedsToFetchPerPage, page: state.currentPage }
        });

        data = response.data;
        // Clear the fetch error if there was one now that we have good results
        if (state.fetchError) commit("setFetchError", false);
      } catch (e) {
        // 4xx and 5xx errors
        commit("setFetchError", true);
      }

      // No data; Either from error or null results from the database
      // Clear and reset items; then exit
      if (!data.total) {
        commit("setTotalResults", 0);
        commit("setItems", []);
        commit("setIsFetchingSeeds", false);
        return;
      }

      if (!data.seeds) {
        commit("setNoMoreResults", true);
        commit("setIsFetchingSeeds", false);
        return;
      }

      const flattened = flattenSeeds(data.seeds || []);
      commit("addSeeds", flattened);

      commit("incrementPage");
      commit("setTotalResults", data.total);
      commit("setIsFetchingSeeds", false);
    },
    shuffle({ commit, state }) {
      const shuffled = state.items
        .map(a => [Math.random(), a])
        .sort((a, b) => a[0] - b[0])
        .map(a => a[1]);

      commit("setItems", shuffled);
    },
    // This is triggered by a watch function on the filters/filterOptions vuex getter
    // Any change to the filterOptions will reset the pagination and request new data
    // based on the filters / sorting in place
    resetPagination({ commit, dispatch }) {
      commit("setNoMoreResults", false);
      commit("resetPagination");
      commit("setItems", []);
      dispatch("getSeeds");
    }
  }
};

function flattenSeeds(seeds) {
  const flatSeeds = [];

  for (const seed of seeds) {
    const flatSeed = {};
    for (const prop in seed) {
      if (Object.prototype.hasOwnProperty.call(seed, prop)) {
        const value = seed[prop];

        if (typeof value === "object") {
          flatSeed[prop + "_group"] = value.total;
          for (const prop2 in value) {
            if (Object.prototype.hasOwnProperty.call(value, prop2)) {
              if (prop2 !== "total") {
                flatSeed[prop2] = value[prop2];
              }
            }
          }
        } else {
          flatSeed[prop] = seed[prop];
        }
      }
    }
    flatSeed.data = seed;
    flatSeeds.push(flatSeed);
  }

  return flatSeeds;
}
