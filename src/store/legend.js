export default {
  state: {
    enabled: false
  },

  mutations: {
    toggleLegend(state, payload) {
      if (typeof payload === "boolean") {
        state.enabled = payload;
      } else {
        state.enabled = !state.enabled;
      }
    }
  },

  actions: {
    toggleLegend({ commit }, payload) {
      commit("toggleLegend", payload);
    }
  }
};
